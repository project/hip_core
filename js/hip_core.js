(function ($) {

Drupal.hip_core = Drupal.hip_core || {};

Drupal.behaviors.hip_core = {
  attach: function(context) {
    if ($('body').hasClass('hip-admin-pane')) {
      $('body').css('padding-right', 300);
    }

    if (window.parent.document && $('body', window.parent.document).hasClass('hip-admin-pane')) {
      $('body').css('padding-right', 300);
    }
  }
}

})(jQuery);
