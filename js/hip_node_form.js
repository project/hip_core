(function ($) {

Drupal.hipNodeForm = Drupal.hipNodeForm || {};

var placeholder_text = Drupal.t('Enter heading here');

/**
 * Init node form behaviors.
 */
Drupal.behaviors.hipNodeForm = {
  attach: function(context) {
    // Add class to heading fields.
    $('div[id*="heading"].field-type-text input.form-text').each(function(){
      $self = $(this);
      $self.addClass('form-heading');

      // Add placeholder text.
      if ($self.val() == '') {
        $self.val(placeholder_text);
        $self.addClass('form-placeholder-text');
      }

      // Listen to focus event on field.
      $self.bind('focus', function(e) {
        Drupal.hipNodeForm.focus(e, $(this));
      });

      // Listen to blur event on field.
      $self.bind('blur', function(e) {
        Drupal.hipNodeForm.blur(e, $(this));
      });
    });

    // Add node reference buttons.
    $('.form-autocomplete', context).once(null, Drupal.hipNodeForm.init);
  },

  detach: function(context) {
    // Remove placeholder text values.
    $('input.form-placeholder-text').val('');
  }
};

/**
 * React on focus event for heading fields.
 */
Drupal.hipNodeForm.focus = function(e, $self) {
  if ($self.hasClass('form-placeholder-text')) {
    $self.val('');
    $self.removeClass('form-placeholder-text');
  }
}

/**
 * React on blur event for heading fields.
 */
Drupal.hipNodeForm.blur = function(e, $self) {
  if ($self.val() == placeholder_text || $self.val() == '') {
    $self.val(placeholder_text);
    $self.addClass('form-placeholder-text');
  }
  else {
    // Populate the default value for title if empty.
    $title = $('#edit-title');
    if ($title.val() == '') {
      $('#edit-title').val($self.val());
    }
  }
}

})(jQuery);