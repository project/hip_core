<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function hip_promo_user_default_permissions() {
  $permissions = array();

  // Exported permission: create hip_promo content
  $permissions['create hip_promo content'] = array(
    'name' => 'create hip_promo content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: delete any hip_promo content
  $permissions['delete any hip_promo content'] = array(
    'name' => 'delete any hip_promo content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: delete own hip_promo content
  $permissions['delete own hip_promo content'] = array(
    'name' => 'delete own hip_promo content',
    'roles' => array(),
  );

  // Exported permission: edit any hip_promo content
  $permissions['edit any hip_promo content'] = array(
    'name' => 'edit any hip_promo content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: edit own hip_promo content
  $permissions['edit own hip_promo content'] = array(
    'name' => 'edit own hip_promo content',
    'roles' => array(),
  );

  return $permissions;
}
