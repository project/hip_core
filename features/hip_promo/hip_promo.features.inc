<?php

/**
 * Implementation of hook_node_info().
 */
function hip_promo_node_info() {
  $items = array(
    'hip_promo' => array(
      'name' => t('Promo'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Administrative title'),
      'help' => '',
    ),
  );
  return $items;
}
