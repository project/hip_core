<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function hip_form_user_default_permissions() {
  $permissions = array();

  // Exported permission: access all webform results
  $permissions['access all webform results'] = array(
    'name' => 'access all webform results',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'editor',
    ),
  );

  // Exported permission: access own webform results
  $permissions['access own webform results'] = array(
    'name' => 'access own webform results',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: access own webform submissions
  $permissions['access own webform submissions'] = array(
    'name' => 'access own webform submissions',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: create hip_form content
  $permissions['create hip_form content'] = array(
    'name' => 'create hip_form content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: delete all webform submissions
  $permissions['delete all webform submissions'] = array(
    'name' => 'delete all webform submissions',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'editor',
    ),
  );

  // Exported permission: delete any hip_form content
  $permissions['delete any hip_form content'] = array(
    'name' => 'delete any hip_form content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: delete own hip_form content
  $permissions['delete own hip_form content'] = array(
    'name' => 'delete own hip_form content',
    'roles' => array(),
  );

  // Exported permission: delete own webform submissions
  $permissions['delete own webform submissions'] = array(
    'name' => 'delete own webform submissions',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit all webform submissions
  $permissions['edit all webform submissions'] = array(
    'name' => 'edit all webform submissions',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'editor',
    ),
  );

  // Exported permission: edit any hip_form content
  $permissions['edit any hip_form content'] = array(
    'name' => 'edit any hip_form content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: edit own hip_form content
  $permissions['edit own hip_form content'] = array(
    'name' => 'edit own hip_form content',
    'roles' => array(),
  );

  // Exported permission: edit own webform submissions
  $permissions['edit own webform submissions'] = array(
    'name' => 'edit own webform submissions',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  return $permissions;
}
