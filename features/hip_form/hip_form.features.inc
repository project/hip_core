<?php

/**
 * Implementation of hook_node_info().
 */
function hip_form_node_info() {
  $items = array(
    'hip_form' => array(
      'name' => t('Form'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Administrative title'),
      'help' => '',
    ),
  );
  return $items;
}
