<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function hip_news_user_default_permissions() {
  $permissions = array();

  // Exported permission: delete any hip_news content
  $permissions['delete any hip_news content'] = array(
    'name' => 'delete any hip_news content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: delete own hip_news content
  $permissions['delete own hip_news content'] = array(
    'name' => 'delete own hip_news content',
    'roles' => array(),
  );

  // Exported permission: edit any hip_news content
  $permissions['edit any hip_news content'] = array(
    'name' => 'edit any hip_news content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: edit own hip_news content
  $permissions['edit own hip_news content'] = array(
    'name' => 'edit own hip_news content',
    'roles' => array(),
  );

  return $permissions;
}
