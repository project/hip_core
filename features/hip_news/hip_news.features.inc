<?php

/**
 * Implementation of hook_node_info().
 */
function hip_news_node_info() {
  $items = array(
    'hip_news' => array(
      'name' => t('News'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Administrative title'),
      'help' => '',
    ),
  );
  return $items;
}
