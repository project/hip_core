<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function hip_page_user_default_permissions() {
  $permissions = array();

  // Exported permission: create hip_page content
  $permissions['create hip_page content'] = array(
    'name' => 'create hip_page content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: delete any hip_page content
  $permissions['delete any hip_page content'] = array(
    'name' => 'delete any hip_page content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: delete own hip_page content
  $permissions['delete own hip_page content'] = array(
    'name' => 'delete own hip_page content',
    'roles' => array(),
  );

  // Exported permission: edit any hip_page content
  $permissions['edit any hip_page content'] = array(
    'name' => 'edit any hip_page content',
    'roles' => array(
      '0' => 'editor',
    ),
  );

  // Exported permission: edit own hip_page content
  $permissions['edit own hip_page content'] = array(
    'name' => 'edit own hip_page content',
    'roles' => array(),
  );

  return $permissions;
}
