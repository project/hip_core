<?php

/**
 * Implementation of hook_node_info().
 */
function hip_page_node_info() {
  $items = array(
    'hip_page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Administrative title'),
      'help' => '',
    ),
  );
  return $items;
}
