<?php

/**
 * Module settings form.
 */
function hip_core_settings() {
  $form = array('#tree' => TRUE);

  // Wysiwyg buttons.
  if (module_exists('wysiwyg')) {
    // Default Wysiwyg buttons.
    $default_buttons = array(
      'filterd_html' => 'Bold,Italic,Strike,-,Link,Unlink',
      'full_html' => 'Format,-,Bold,Italic,Strike,-,BulletedList,NumberedList,Blockquote,-,Link,Unlink,-,JustifyLeft,JustifyCenter,JustifyRight,-,PasteText,RemoveFormat,-,Maximize,-,Source',
      'plain_text' => '',
    );

    $form['hip_wysiwyg_buttons'] = array(
      '#type' => 'fieldset',
      '#title' => t('Wysiwyg buttons'),
      '#description' => t('Set default Wysiwyg buttons per profile.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $profiles = wysiwyg_profile_load_all();
    $wysiwyg_buttons_defaults = variable_get('hip_wysiwyg_buttons', $default_buttons);
    
    foreach ($profiles as $profile) {
      $format = filter_format_load($profile->format);

      // Default value.
      if (isset($wysiwyg_buttons_defaults[$profile->format])) {
        $default = $wysiwyg_buttons_defaults[$profile->format];
      }
      else {
        $default = NULL;
      }
      
      $form['hip_wysiwyg_buttons'][$profile->format] = array(
        '#type' => 'textarea',
        '#title' => $format->name,
        '#description' => t('Enter the name of the buttons, separated by commas, you want available in the Wysiwyg editor. Use a hyphen character to create sets of buttons.'),
        '#default_value' => $default,
        '#rows' => 3,
      );
    }
  }

  return system_settings_form($form);
}